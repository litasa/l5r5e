# Updating - Bests practices

- Anytime you update to a major version make a backup of foundry's data directory (default : `%localappdata%/FoundryVTT/data/`).
- Take time to upgrading to a major version (ex FoundryVTT v9->v10).<br>A lot of bugs can be on firsts patchs, and a lots of systems/modules won't upgrade fast and will be incompatible or not tested (a lot of us do it only on our free time).<br>If you need some timing windows: let at least 2 weeks to 1 month.
