# Roll n Keep (RnK)
The RnK use `ChatMessage` to retrieve the roll, alter it, add the new message and delete the old.

> If you have any idea how to modify directly the ChatMessage and update it, let me know.

Usage :
```js
new RollnKeepDialog(messageId).render(true);
```
